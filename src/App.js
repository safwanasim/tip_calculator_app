import './App.css';
import Bill from './components/Bill.jsx';
import TipButtons from './components/TipButtons.jsx';
import NumberOfPeople from './components/NumberOfPeople';
import TipAmount from './components/TipAmount.jsx';
import Total from './components/Total.jsx';
import Reset from './components/reset.jsx';
import React, { Component } from 'react';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      billValue: '',
      numberOfPeople: '',
      tipAmount: '$0.00',
      totalAmount: '$0.00',
      percentage: '',
      custom: '',
      buttonColor: '',
    };
  }
  getBillValue = (e) => {
    if (e.target.value === '' || e.target.value < 0) {
      this.setState({ billValue: '' }, () => {
        this.setState({ tipAmount: '$0.00', totalAmount: '$0.00' });
      });
    } else {
      this.setState({ billValue: Number(e.target.value) }, () => {
        if (this.state.percentage !== '' && this.state.billValue !== '') {
          this.calculateTip(this.state.numberOfPeople, this.state.billValue);
        }
      });
    }
  };

  getPercentage = (e) => {
    e.target.style.background = 'red'; // I know what I am doing, this only works once (not focused on UI)
    this.setState({ percentage: e.target.value }, () => {
      if (this.state.numberOfPeople !== '' && this.state.billValue !== '') {
        this.calculateTip(this.state.numberOfPeople, this.state.billValue);
      }
    });
  };

  handleNumberOfInputChange = (e) => {
    if (e.target.value === '' || e.target.value < 0) {
      this.setState({ numberOfPeople: '' }, () => {
        this.setState({ tipAmount: '$0.00', totalAmount: '$0.00' });
      });
    } else {
      this.setState({ numberOfPeople: Number(e.target.value) }, () => {
        if (this.state.numberOfPeople !== '' && this.state.billValue !== '') {
          this.calculateTip(this.state.numberOfPeople, this.state.billValue);
        }
      });
    }
  };

  getCustomValue = (e) => {
    if (e.target.value === '' || e.target.value < 0) {
      this.setState({ custom: '' });
    } else {
      this.setState({ custom: e.target.value }, () => {
        if (this.state.numberOfPeople !== '' && this.state.billValue !== '') {
          this.calculateTip(this.state.numberOfPeople, this.state.billValue);
        }
      });
    }
  };

  reset = () => {
    this.setState({
      billValue: '',
      numberOfPeople: '',
      tipAmount: '$0.00',
      totalAmount: '$0.00',
      percentage: 0,
      custom: '',
      buttonColor: 'rgb(0, 73, 77)',
    });
  };

  calculateTip = (ppl, amt) => {
    let percentage = this.state.percentage;
    if (this.state.custom !== '') {
      percentage = this.state.custom;
    }
    if (amt >= 0 && ppl >= 1) {
      let totalTip = (percentage * amt) / 100;
      let totalAmt = amt + totalTip;
      let perPersonTip = (totalTip / ppl).toFixed(2);
      let totalAmtPerPerson = (totalAmt / ppl).toFixed(2);
      console.log(perPersonTip, totalAmtPerPerson);
      this.setState({ tipAmount: '$' + perPersonTip, totalAmount: '$' + totalAmtPerPerson });
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="left-container">
          <Bill value={this.state.billValue} getBillValue={this.getBillValue} />
          <TipButtons
            getPercentage={this.getPercentage}
            customValue={this.state.custom}
            getCustomValue={this.getCustomValue}
            color={this.state.buttonColor}
          />
          <NumberOfPeople
            value={this.state.numberOfPeople}
            handleNumberOfInputChange={this.handleNumberOfInputChange}
          />
        </div>
        <div className="right-container">
          <TipAmount tipAmount={this.state.tipAmount} />
          <Total totalAmount={this.state.totalAmount} />
          <Reset reset={this.reset} />
        </div>
      </React.Fragment>
    );
  }
}

export default App;
