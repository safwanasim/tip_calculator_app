import React from 'react';
// import ReactDOM from 'react-dom';

const css = `
.tip input {
    width: 5rem;
    height: 2rem;
    font-size: 1.2rem;
    border-radius: 4px;
    text-align: center;
    border: none;
    margin-top: 0.5rem;
    background: hsl(189, 41%, 97%);
    color: hsl(183, 100%, 15%);
    font-family: 'Space Mono', monospace;
    display: block;
  }
  .tip button {
    background: rgb(0, 73, 77);
    border: none;
    font-size: 1rem;
    text-align: center;
    padding: 0.5rem 2rem;
    color: white;
    display: inline-block;
    border-radius: 4px;
    font-family: 'Space Mono', monospace;
  }`;

const TipButtons = (props) => {
  return (
    <div className="tip">
      <style>{css}</style>
      <p>Select Tip %</p>
      <div
        onClick={(e) => {
          props.getPercentage(e);
        }}
      >
        <button
          className="tip button box"
          style={{ background: props.color }}
          type="button"
          value="5"
          id="tip5"
        >
          5%
        </button>
        <button
          style={{ background: props.color }}
          className="tip button box"
          type="button"
          value="10"
          id="tip10"
        >
          10%
        </button>
        <button
          style={{ background: props.color }}
          className="tip button box"
          type="button"
          value="15"
          id="tip15"
        >
          15%
        </button>
        <button
          style={{ background: props.color }}
          className="tip button box"
          type="button"
          value="25"
          id="tip25"
        >
          25%
        </button>
        <button
          style={{ background: props.color }}
          className="tip button box"
          type="button"
          value="50"
          id="tip50"
        >
          50%
        </button>
      </div>
      <input
        onChange={(e) => {
          props.getCustomValue(e);
        }}
        className="tip button"
        type="number"
        id="custom"
        placeholder="Custom%"
        value={props.customValue}
      />
    </div>
  );
};

export default TipButtons;
