import React from 'react';

const TipAmount = (props) => {
  return (
    <div class="tipAmount">
      <p id="tPP">TipAmount {props.tipAmount}/person</p>
    </div>
  );
};

export default TipAmount;
