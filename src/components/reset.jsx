import React from 'react';

export default function reset(props) {
  return (
    <div class="reset">
      <button onClick={props.reset} type="button" id="reset">
        Reset
      </button>
    </div>
  );
}
