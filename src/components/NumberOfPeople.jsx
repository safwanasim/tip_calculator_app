import React from 'react';

const css = `.people input {
    width: 24%;
    border-radius: 4px;
    border: none;
    background-image: url(images/icon-person.svg);
    background-repeat: no-repeat;
    background-position-y: center;
    background-color: hsl(189, 41%, 97%);
    color: hsl(183, 100%, 15%);
    text-align: right;
    font-size: 1.5rem;
    outline: none;
  }`;

const NumberOfPeople = (props) => {
  return (
    <div class="people">
      <style>{css}</style>
      <p>Number of People</p>
      <input
        type="number"
        onChange={(e) => props.handleNumberOfInputChange(e)}
        id="NumberOfPeople"
        placeholder="0"
        value={props.value}
      />
    </div>
  );
};
export default NumberOfPeople;
