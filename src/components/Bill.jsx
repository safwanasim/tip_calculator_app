import React from 'react';

const Bill = (props) => {
  return (
    <React.Fragment>
      <p>Bill</p>
      <input
        onChange={(e) => {
          props.getBillValue(e);
        }}
        type="number"
        name="Bill"
        placeholder="0"
        id="Bill"
        value={props.value}
      />
    </React.Fragment>
  );
};

export default Bill;
