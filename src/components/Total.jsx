import React from 'react';

export default function Total(props) {
  return (
    <div class="total">
      <p id="tot">Total {props.totalAmount}/person</p>
    </div>
  );
}
